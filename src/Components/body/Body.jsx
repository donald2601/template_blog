import React from 'react';
import TitleArticle from './TitleArticle';
import ArticlePopu from './ArticlePopu';
import AutresArticles from './AutresArticles';

const Body = () => {
    return (
        <div className='container mt-3'>
            <TitleArticle/>
            <ArticlePopu/>
            <AutresArticles/>
        </div>
    );
}

export default Body;
