import React from 'react';
import AboutUs from './sideBar/AboutUs';
import Archives from './sideBar/Archives';
import Articles from './Articles';

const AutresArticles = () => {
    return (
        <div className='row g-5'>
            <div className='col-md-8'>
                <Articles/>
            </div>
            <div className='col-md-4'>
            <div className="position-sticky" style={{top : '2rem'}}>
                <AboutUs />
                <Archives />
            </div>
            </div>
        </div>
    );
}

export default AutresArticles;
