import './App.css';
import Navbar from './Components/Navbars/Navbar';
import Body from './Components/body/Body';
import Footer from './Components/footer/Footer';

function App() {
  return (
    <div className="App">
      <Navbar/>
      <Body/>
      {/* <Footer/> */}
    </div>
  );
}

export default App;
